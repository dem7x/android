package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage extends Selectors {

    protected AppiumDriver driver;

    protected BasePage(AppiumDriver driver) {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    protected void visibilityOf(WebElement locator) {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.visibilityOf(locator));
    }

    protected void visibilityOf(WebElement locator, long time) {
        WebDriverWait wait = new WebDriverWait(driver, time);
        wait.until(ExpectedConditions.visibilityOf(locator));
    }

    protected void swipeVertical(double startPercentage, double finalPercentage, double anchorPercentage) {
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * finalPercentage);
        new TouchAction(driver)
                .press(anchor, endPoint)
                .waitAction()
                .moveTo(anchor, startPoint)
                .release()
                .perform();
    }

    protected void swipeUntilElementFound(By element, double startPercentage,
                                          double finalPercentage, double anchorPercentage) {
        boolean isElementDisplayed = driver.findElements(element).size() > 0;
        long start = System.currentTimeMillis() / 1000,
                timeout = 15, currentTime = start;
        while (!isElementDisplayed && (start + timeout > currentTime)) {
            swipeVertical(startPercentage, finalPercentage, anchorPercentage);
            isElementDisplayed = driver.findElements(element).size() > 0;
            currentTime = System.currentTimeMillis() / 1000;
        }
    }

    protected void tap(WebElement element) {
        visibilityOf(element);
        new TouchAction(driver).tap(element).perform();
    }

    protected void swipeRightToLeft(WebElement element, int startX, int startY) {
        TouchAction touchAction = new TouchAction(driver);
        int endX = element.getLocation().getX();
        int endY = element.getLocation().getY();
        System.out.println("Move from [" + startX + "," + startY + "] to " +
                "[" + endX + "," + endY + "]");
        touchAction.press(element, startX, startY)
                .waitAction()
                .moveTo(endX, endY)
                .release()
                .perform();
    }

    protected void longClick(WebElement webElement) {
        TouchAction Action = new TouchAction(driver);
        Action.longPress(webElement).release().perform();
    }

    public void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void zoom(WebElement webElement, int finger1X, int finger1Y, int finger2X, int finger2Y) {
        Dimension size = webElement.getSize();
        int rightCornerX = (int) (size.width * 0.9);
        int rightCornerY = (int) (size.height * 0.2);
        int leftCornerX = (int) (size.height * 0.2);
        int leftCornerY = (int) (size.height * 0.5);
        TouchAction finger1 = new TouchAction(driver);
        finger1.press(webElement, rightCornerX, rightCornerY)
                .waitAction(Duration.ofMillis(1500))
                .moveTo(rightCornerX - finger1X, rightCornerY + finger1Y)
                .release();
        TouchAction finger2 = new TouchAction(driver);
        finger2.press(webElement, leftCornerX, leftCornerY)
                .waitAction(Duration.ofMillis(1500))
                .moveTo(leftCornerX + finger2X, leftCornerY - finger2Y)
                .release();
        MultiTouchAction multiTouchAction = new MultiTouchAction(driver);
        multiTouchAction.add(finger1).add(finger2);
        multiTouchAction.perform();
    }
}
