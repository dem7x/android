package pages;

public class Selectors {

    //credentials
    protected final String correctUsername = "maksym.zaselian@gmail.com";
    protected final String correctPassword = "Abcd123$";
    protected final String incorrectMail = "incorrectMail";

    // field names
    protected final String appPackage = "com.pinterest:id/";
    protected final String emailAddressEditFieldId = "email_address";
    protected final String emailContinueButtonId = "continue_email_bt";
    protected final String passwordEditFieldId = "password";
    protected final String passwordNextButtonId = "next_bt";
    protected final String acceptAlertResourceId = "com.google.android.gms:id/cancel";
    protected final String profileIconId = "profile_icon";
    protected final String imageOnPageId = "pin_image_view";
    protected final String homePageId = "library_header_full_name";
    protected final String settingsIconId = "menu_settings";
    protected final String flashlightSearchButtonId = "flashlight_search_button";
    protected final String visualSearchId = "drawer_header";
    protected final String settingsSwitchToggleXpath = "//android.widget.TextView[@text='Autoplay on mobile data']/../android.widget.Switch";
    protected final String firstImageOnPageXpath = "//android.widget.AdapterView/android.view.View";
    protected final String imageXpath = "//android.widget.ImageView";
    protected final String spinnerId = "loading_pb";

    // AndroidFindBy selectors
    protected final String incorrectMailErrorUiSel = "new UiSelector().text(\"Sorry this doesn't look like a valid email\")";
    protected final String incorrectMailErrorSelendroid = "look like a valid email";
    protected final String boardsSwipeUiSel = "new UiSelector().text(\"Maksym hasn't saved any ideas yet\")";
    protected final String boardsSwipeTextSelendroid = "saved any ideas yet";
    protected final String pinsSwipeUiSel = "new UiSelector().text(\"Want to add some Pins to your boards? Try a search.\")";
    protected final String pinsSwipeTextSelendroid = "Want to add some Pins to your boards? Try a search.";
    protected final String settingsEditSettingsUiSel = "new UiSelector().text(\"Edit settings\")";
    protected final String settingsEditSettingsSelendroid = "Edit settings";
    protected final String settingsSeeTermsAndPrivacyUiSel = "new UiSelector().text(\"See terms and privacy\")";
    protected final String settingsSeeTermsAndPrivacySelendroid = "See terms and privacy";
    protected final String settingsAboutUiSel = "new UiSelector().text(\"About\")";
    protected final String settingsAboutSelendroid = "About";
    protected final String settingsAutoPlayOnMobileDataUiSel = "new UiSelector().text(\"Autoplay on mobile data\")";
    protected final String settingsAutoPlayOnMobileDataSelendroid = "Autoplay on mobile data";
}
