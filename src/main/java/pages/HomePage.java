package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.SelendroidFindBy;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class HomePage extends BasePage {

    @AndroidFindBy(xpath = firstImageOnPageXpath)
    @SelendroidFindBy(xpath = firstImageOnPageXpath)
    public WebElement firstImageOnPage;

    @AndroidFindBy(uiAutomator = boardsSwipeUiSel)
    @SelendroidFindBy(partialLinkText = boardsSwipeTextSelendroid)
    public WebElement boardsSwipeText;

    @AndroidFindBy(uiAutomator = pinsSwipeUiSel)
    @SelendroidFindBy(linkText = pinsSwipeTextSelendroid)
    public WebElement pinsSwipeText;

    @AndroidFindBy(id = appPackage + imageOnPageId)
    @SelendroidFindBy(id = imageOnPageId)
    public WebElement imageOnPage;

    @AndroidFindBy(id = appPackage + profileIconId)
    @SelendroidFindBy(id = profileIconId)
    public WebElement profileIcon;

    @FindBy(id = appPackage + homePageId)
    @SelendroidFindBy(id = homePageId)
    public WebElement homePage;

    @FindBy(id = appPackage + settingsIconId)
    @SelendroidFindBy(id = settingsIconId)
    public WebElement settingsIcon;

    @AndroidFindBy(id = appPackage + flashlightSearchButtonId)
    @SelendroidFindBy(id = flashlightSearchButtonId)
    public WebElement flashlightSearchButton;

    @AndroidFindBy(id = appPackage + visualSearchId)
    @SelendroidFindBy(id = visualSearchId)
    public WebElement visualSearch;

    @AndroidFindBy(id = appPackage + spinnerId)
    @SelendroidFindBy(id = spinnerId)
    public WebElement spinner;

    @AndroidFindBy(xpath = imageXpath)
    @SelendroidFindBy(xpath = imageXpath)
    public WebElement image;

    private Dimension size;

    public HomePage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void tapOnFirstImage() {
        visibilityOf(firstImageOnPage);
        tap(firstImageOnPage);
    }

    public void tapOnFlashlight() {
        visibilityOf(flashlightSearchButton);
        tap(flashlightSearchButton);
    }

    public void checkVisualSearchDisplayed() {
        visibilityOf(visualSearch);
    }

    public boolean isSpinnerAppeared() {
        visibilityOf(spinner);
        return true;
    }

    public void zoomImage(int finger1X, int finger1Y, int finger2X, int finger2Y) {
        zoom(image, finger1X, finger1Y, finger2X, finger2Y);
    }

    public boolean profileIconDisplayed() {
        visibilityOf(profileIcon);
        return true;
    }

    public void tapProfileIcon() {
        visibilityOf(profileIcon);
        tap(profileIcon);
    }

    public WebElement getBoardsText() {
        return boardsSwipeText;
    }

    public WebElement getPinsText() {
        return pinsSwipeText;
    }

    public void swipeFromRightToLeft(int xCoordinates, int yCoordinates) {
        visibilityOf(profileIcon);
        tap(profileIcon);
        visibilityOf(homePage);
        size = getBoardsText().getSize();
        System.out.print("Size is = " + size.width + " " + size.height);
        int startx = size.width * xCoordinates / 100;
        int starty = size.height * yCoordinates / 100;
        swipeRightToLeft(getBoardsText(), startx, starty);
    }

    public SettingsPage goToSettings() {
        visibilityOf(settingsIcon);
        tap(settingsIcon);
        return new SettingsPage(driver);
    }

}
