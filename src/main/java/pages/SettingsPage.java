package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.SelendroidFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SettingsPage extends BasePage {
    
    @AndroidFindBy(uiAutomator = settingsEditSettingsUiSel)
    @SelendroidFindBy(linkText = settingsEditSettingsSelendroid)
    public WebElement settingsEditSettings;
    
    @AndroidFindBy(uiAutomator = settingsSeeTermsAndPrivacyUiSel)
    @SelendroidFindBy(linkText = settingsSeeTermsAndPrivacySelendroid)
    public WebElement settingsSeeTermsAndPrivacy;

    @AndroidFindBy(uiAutomator = settingsAboutUiSel)
    @SelendroidFindBy(linkText = settingsAboutSelendroid)
    public WebElement settingsAbout;

    @AndroidFindBy(xpath = settingsSwitchToggleXpath)
    @SelendroidFindBy(xpath = settingsSwitchToggleXpath)
    public WebElement settingsSwitchToggle;

    public SettingsPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void tapEditSettings() {
        tap(settingsEditSettings);
    }

    public void tapSeeTermsAndPrivacy() {
        tap(settingsSeeTermsAndPrivacy);
    }

    public void tapAbout() {
        tap(settingsAbout);
    }

    public void scrollUntilElementFound() {
        By element = MobileBy.AndroidUIAutomator(settingsAutoPlayOnMobileDataUiSel);
        swipeUntilElementFound(element, 0.2, 0.7, 0.2);
    }

    public void toggleButton() {
        longClick(settingsSwitchToggle);
    }

    public String getToggleText() {
        return settingsSwitchToggle.getText();
    }

}
