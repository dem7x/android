package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.*;
import org.openqa.selenium.WebElement;

public class LoginPage extends BasePage {
    
    @AndroidFindBy(uiAutomator = incorrectMailErrorUiSel)
    @SelendroidFindBy(partialLinkText = incorrectMailErrorSelendroid)
    public WebElement incorrectMailErrorText;

    @AndroidFindBy(id = appPackage + emailAddressEditFieldId)
    @SelendroidFindBy(id = emailAddressEditFieldId)
    public WebElement loginPageEmailAddress;

    @AndroidFindBy(id = appPackage + emailContinueButtonId)
    @SelendroidFindBy(id = emailContinueButtonId)
    public WebElement loginPageContinueButton;

    @AndroidFindBy(id = appPackage + passwordEditFieldId)
    @SelendroidFindBy(id = passwordEditFieldId)
    public WebElement loginPagePassword;

    @AndroidFindBy(id = appPackage + passwordNextButtonId)
    @SelendroidFindBy(id = passwordNextButtonId)
    public WebElement loginPageNextButton;

    @AndroidFindBy(id = acceptAlertResourceId)
    public WebElement acceptAlert;

    public LoginPage(AppiumDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean isErrorMessageAppeared() {
        return incorrectMailErrorText.isDisplayed();
    }

    public void checkAlertExists() {
        try {
            visibilityOf(acceptAlert, 3);
            tap(acceptAlert);
        } catch (Exception e) {
            System.out.print("No alert appeared here. Continue...");
        }
    }

    public LoginPage invalidLogin() {
        visibilityOf(loginPageEmailAddress);
        tap(loginPageEmailAddress);
        checkAlertExists();
        loginPageEmailAddress.sendKeys(incorrectMail);
        tap(loginPageContinueButton);
        return new LoginPage(driver);
    }

    public HomePage successfulLogin() {
        visibilityOf(loginPageEmailAddress);
        tap(loginPageEmailAddress);
        checkAlertExists();
        loginPageEmailAddress.sendKeys(correctUsername);
        tap(loginPageContinueButton);
        visibilityOf(loginPagePassword);
        tap(loginPagePassword);
        loginPagePassword.sendKeys(correctPassword);
        tap(loginPageNextButton);
        return new HomePage(driver);
    }

}
