package basic;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.HomePage;
import pages.LoginPage;
import pages.SettingsPage;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AppiumTest extends AndroidSetup {

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        createAppiumServer();
        prepareDevice();
    }

    @Test
    public void falseLoginTest() {
        LoginPage loginPage = new LoginPage(driver);
        Assert.assertTrue(loginPage
                .invalidLogin()
                .isErrorMessageAppeared());
    }

    @Test
    public void successLoginTest() {
        LoginPage loginPage = new LoginPage(driver);
        Assert.assertTrue(loginPage
                .successfulLogin()
                .profileIconDisplayed());
    }

    @Test
    public void checkPinsOnHomepageTest() {
        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = loginPage.successfulLogin();
        homePage.swipeFromRightToLeft(70, 63);
        Assert.assertTrue(homePage
                .getPinsText()
                .isDisplayed());
    }

    @Test
    public void switchAutoPlayOnMobileDataTest() {
        HomePage homePage = new LoginPage(driver).successfulLogin();
        homePage.tapProfileIcon();
        SettingsPage settingsPage = homePage.goToSettings();
        settingsPage.tapEditSettings();
        settingsPage.scrollUntilElementFound();
        settingsPage.toggleButton();
        Assert.assertTrue(settingsPage
                .getToggleText()
                .equals("OFF"));
    }

    @Test
    public void visualSearchTest() {
        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = loginPage.successfulLogin();
        homePage.tapOnFirstImage();
        homePage.tapOnFlashlight();
        homePage.checkVisualSearchDisplayed();
        homePage.sleep(3);
        homePage.zoomImage(50, 50, 50, 50);
        Assert.assertTrue(homePage.isSpinnerAppeared());
    }

    @AfterMethod(alwaysRun = true)
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            System.out.println(testResult.getStatus());
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            File scrFile = driver.getScreenshotAs(OutputType.FILE);
            File currentDir = new File("");
            System.out.print("Current path is " + currentDir.getAbsolutePath());
            FileUtils.copyFile(scrFile, new File(currentDir.getAbsolutePath() + "/screenshots/" +
                    testResult.getName() + "-" +
                    dateFormat.format(new Date()) + ".jpg"));
        }
        driver.resetApp();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        stopAppiumServer();
    }

}

