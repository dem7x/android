package basic;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.remote.DesiredCapabilities;
import pages.Selectors;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class AndroidSetup extends Selectors {

    protected AndroidDriver driver;
    private AppiumDriverLocalService service;

    protected void createAppiumServer() {
        service = AppiumDriverLocalService.buildDefaultService();
        service.start();
    }

    protected void stopAppiumServer() {
        service.stop();
    }

    protected void prepareDevice() {
        File currentDir = new File("");
        String appPath = currentDir.getAbsolutePath() + "/src/main/resources/apps/" + System.getProperty("app");
        System.out.print("Path is = " + appPath);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("appium-version", "1.7.1");
        capabilities.setCapability("automationName", System.getProperty("autoname"));
        capabilities.setCapability("platformVersion", System.getProperty("platformVersion"));
        capabilities.setCapability("platformName", System.getProperty("platformName"));
        capabilities.setCapability("deviceName", System.getProperty("deviceName"));
        capabilities.setCapability("udid", System.getProperty("udid"));
        capabilities.setCapability("avd", System.getProperty("avd"));
        capabilities.setCapability("avdLaunchTimeout", 60000);
        capabilities.setCapability("avdReadyTimeout", 60000);
        capabilities.setCapability("newCommandTimeout", 600);
        capabilities.setCapability("app", appPath);
        capabilities.setCapability("clearSystemFiles", true);
        capabilities.setCapability("appPackage", System.getProperty("appPackage"));
        capabilities.setCapability("appActivity", System.getProperty("appActivity"));
        try {
            driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
